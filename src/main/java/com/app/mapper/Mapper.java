package com.app.mapper;

import com.app.model.Student;
import org.apache.ibatis.annotations.*;

import java.util.List;

@org.apache.ibatis.annotations.Mapper
public interface Mapper {
    @Insert("INSERT INTO STUDENT(id, name, age, email) VALUES (#{id},#{name},#{age},#{email})")
    public void add(Student student);

    @Update("UPDATE STUDENT SET name=#{name},age=#{age},email=#{email} WHERE id=#{id}")
    public void update(Student student);

    @Delete("DELETE FROM STUDENT WHERE id=#{id}")
    public void delete(Long id);

    @Select("SELECT * FROM STUDENT WHERE id=#{id}")
    public Student getById(Long id);

    @Select("SELECT * FROM STUDENT")
    public List<Student> getAll();
}
