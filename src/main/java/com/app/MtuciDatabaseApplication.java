package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MtuciDatabaseApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(MtuciDatabaseApplication.class, args);
    }
}
