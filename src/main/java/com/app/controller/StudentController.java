package com.app.controller;

import com.app.model.Student;
import com.app.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @RequestMapping("/add")
    @ResponseBody
    public String add(Student student) {
        try {
            studentService.add(student);
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
        return "success";
    }

    @RequestMapping("/update")
    @ResponseBody
    public String update(Student student) {
        try {
            studentService.update(student);
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
        return "success";
    }

    @RequestMapping("/delete")
    @ResponseBody
    public String delete(Long id) {
        try {
            studentService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
        return "success";
    }

    @RequestMapping("/getById")
    @ResponseBody
    public Student getById(Long id){
        try {
            return studentService.getById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Student();
    }

    @RequestMapping("/getAll")
    @ResponseBody
    public List<Student> getAll(){
        try {
            return studentService.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<Student>();
    }
}