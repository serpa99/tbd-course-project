package com.app.service.imp;

import com.app.mapper.Mapper;
import com.app.model.Student;
import com.app.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImp implements StudentService {

    @Autowired
    private Mapper mapper;

    @Override
    public void add(Student student) {
        mapper.add(student);
    }

    @Override
    public void update(Student student) {
        mapper.update(student);
    }

    @Override
    public void delete(Long id) {
        mapper.delete(id);
    }

    @Override
    public Student getById(Long id) {
        return mapper.getById(id);
    }

    @Override
    public List<Student> getAll() {
        return mapper.getAll();
    }
}