package com.app.service;

import com.app.model.Student;

import java.util.List;

public interface StudentService {
    public void add(Student student);
    public void update(Student student);
    public void delete(Long id);

    public Student getById(Long id);
    public List<Student> getAll();
}